//对axios进行二次封装
import axios from 'axios';
// import store from '@/store';
axios.defaults.withCredentials=true;
//1.利用axios方法去创建axios实例
const requests=axios.create({
    //配置对象
    baseURL:'/api',
    timeout:5000,
    headers:{},
    withCredentials:true,
    crossDomain:true,
});
//请求拦截器
requests.interceptors.request.use(
    config => {
        // if (localStorage.getItem("satoken")) {
            config.headers.satoken= localStorage.getItem("satoken");
            // console.log(1111)
          // }
    //   console.log('拦截器1')
      return config
    },
    error => {
      console.log('错误') // for debug
      return Promise.reject(error)
    }
  )

//响应拦截器
requests.interceptors.response.use((res)=>{
    return res.data
},(err)=>{
    return Promise.reject(new Error('faile'))//终结promise链
})  

//对外暴露
export default requests;