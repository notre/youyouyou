import request from './request'

export const reqlogin=(data)=>{
    return request({
        method:'POST',
        url:`userInfo/phone/login?phone=${data.phone}&password=${data.password}`,
        data,
    })
}


export const reqcode=(data)=>{
    return request({
        method:'GET',
        url:`api/sendSmsCode?phone=${data.phone}&pattern=${data.pattern}`,
        data,
    })
}

export const reqregister=(data)=>{
    return request({
        method:'POST',
        url:`userInfo/register?phone=${data.phone}&code=${data.code}&password=${data.password}`,
        data,
    })
}

export const reqchange=(data)=>{
    return request({
        method:'POST',
        url:`userInfo/resetPassword?phone=${data.phone}&password=${data.password}&code=${data.code}`,
        data,
    })
}

export const reqout=(data)=>{
    return request({
        method:'GET',
        url:`userInfo/logout?userId=${data.userId}`,
        data,
    })
}


export const reqpushcard=(data)=>{
    return request({
        method:'GET',
        url:`consumer/signIn?userId=${data}`,
        data,
    })
}


export const reqcardata=(data)=>{
    return request({
        method:'GET',
        url:`consumer/getSignIn?userId=${data}`,
        data,
    })
}

export const reqiscard=(data)=>{
    return request({
        method:'GET',
        url:`consumer/getSignStatus?userId=${data}`,
        data,
    })
}


export const  reqoutlogin=(data)=>{
    return request({
        method:'GET',
        url:`userInfo/logout?userId=${data}`,
        data,
    })
}


export const reqgetmessagebyId=(data)=>{
    return request({
        method:'GET',
        url:`userInfo/userInfoById?userId=${data}`,
        data,
    })
}



export const reqchangemessage=(data)=>{
    return request({
        method:'POST',
        url:`userInfo/update`,
        data,
    })
}

export const reqexportexcel=(data)=>{
    return request({
        method:'POST',
        url:`consumer/sendInfoToEmail?userId=${data.userId}&startDissipate=${data.startDissipate}&endDissipate=${data.endDissipate}&sign=${data.sign}&email=${data.email}`,
        data,
    })
}


export const reqwriteaccounts=(data)=>{
    return request({
        method:'POST',
        url:`consumer/enter`,
        data,
    })
}

export const reqpicturecute=(data)=>{
    return request({
        method:'GET',
        url:`api/correct?userId=${data.userId}&pictureURL=${data.pictureURL}`,
        data,
    })
}

export const reqpictureurl=(data)=>{
    return request({
        method:'POST',
        url:`api/uploadImg`,
        data,
        headers: {'Content-Type': 'multipart/form-data'},
    })
}

