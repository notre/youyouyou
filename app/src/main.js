import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from '@/store'
import Vant from 'vant';
import '../src/svg/index'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import 'vant/lib/index.css';
Vue.use(Vant);
Vue.use(MintUI)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router,
  store,
  beforeCreate() {
    Vue.prototype.$bus = this//安装全局事件总线
}
}).$mount('#app')
