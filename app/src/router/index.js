//引入Vue|Vue-router
import Vue from 'vue'
import VueRouter from 'vue-router'

//使用路由插件
Vue.use(VueRouter)
import acc from '@/components/accounts'
import dia  from '@/components/diagram'
import home from '@/components/home'
import personal from '@/components/own'
import login from '@/components/login'
import search from '@/components/retrieval'
import register from '@/components/register'
import returnp from '@/components/retrieve'
let router= new VueRouter({
    routes:[
        {
            path:'/accounts',
            component:acc
        },
        {
            path:'/diagram',
            component:dia
        },
        {
            path:'/home',
            component:home
        },
        {
            path:'/personal',
            component:personal
        },
        {
            path:'/login',
            component:login
        },
        {
            path:'/retrieval',
            component:search
        },
        {
            path:'/',
            component:login
        },
        {
            path:'/register',
            component:register
        },
        {
            path:'/retrieve',
            component:returnp
        }
    ]
})


export default router
