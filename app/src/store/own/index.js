import {reqpushcard,reqcardata,reqiscard,reqoutlogin,reqgetmessagebyId,reqchangemessage,reqexportexcel} from '@/api/index';
 

const state = {
    pushok:'',
    iscard:'',
    cardata:{},
    erdata:'',
    outlo:'',
    idmessage:{},
    ischange:'',
    isexcel:''
}

const mutations = {
    PUSHC(state,value){
        state.pushok=value
    },
    CARDATA(state,value){
        state.cardata=value
    },
    ISCARD(state,value){
        state.iscard=value
    },
    ERDATA(state,value){
        state.erdata=value
    },
    LOGINOUT(state,value){
        state.outlo=value
    },
    GETMBID(state,value){
        state.idmessage=value
    },
    CHANGEMES(state,value){
        state.ischange=value
    },
    GETEXCEL(state,value){
        state.isexcel=value
    }

}

const actions = {
      async pushc({commit},value){
        let result=await reqpushcard(value);
        console.log(result)
        if(result.code==0){
            commit('PUSHC','ok')
        }else{
            commit('ERDATA',result.msg)
        }
      },
      async cardata({commit},value){
        let result=await reqcardata(value);
        // console.log(result);
        if(result.code==0){
            commit('CARDATA',result.data)
        }else{
            commit('CARDATA',result.msg)
        }
      },
      async iscard({commit},value){
        let result=await reqiscard(value);
        // console.log(result)
        if(result.data=='yes'){
            commit('ISCARD','ok')
        }else{
            commit('ISCARD','err')
        }
      },
      async loginout({commit},value){
        let result=await reqoutlogin(value);
        console.log(result);
        if(result.msg=='success'){
            commit('LOGINOUT',result.msg)
        }
      },
      async getmbiId({commit},value){
        let result=await reqgetmessagebyId(value);
        // console.log(result);
        if(result.msg=='success'){
            commit('GETMBID',result.data)
        }
      },
      async changemes({commit},value){
        let result=await reqchangemessage(value);
        if(result.msg=='success'){
            commit('CHANGEMES','ok')
        }
      },
      async getexcel({commit},value){
        let result=await reqexportexcel(value);
        console.log(result);
        if(result.msg=='success'){
            commit('GETEXCEL','ok')
        }
      }
}

export default {
    state,
    mutations,
    actions
}

