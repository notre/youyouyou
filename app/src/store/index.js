import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex)
//引入小仓库
import login from './login/index'
import own from './own/index'
import account from './accounts/index'
export default new Vuex.Store({
    //实现模块式开发
    modules:{
        login,
        own,
        account,
    }
})