import {reqwriteaccounts,reqpicturecute,reqpictureurl} from '@/api/index';
 

const state = {
    qaccount:'',
}

const mutations = {
    REQWRITEACC(state,value){
        state.qaccount=value
    }

}

const actions = {
        async reqwriteacc({commit},value){
            let result=await  reqwriteaccounts(value);
            console.log(result);
            if(result.msg=='success'){
                commit('REQWRITEACC','ok')
            }else{
                commit('REQWRITEACC',result.msg)
            }
        },
        async reqpicture({commit},value){
            let result=await reqpicturecute(value);
            console.log(result)
        },
        async requrl({commit},value){
            let result=await  reqpictureurl(value);
            console.log(result)
        }
}

export default {
    state,
    mutations,
    actions
}

