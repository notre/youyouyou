import {reqlogin,reqcode,reqregister,reqchange} from '@/api/index';
 

const state = {
   logindata:{},
   codeerror:'',
   registererr:'',
   retrieveok:''
}

const mutations = {
    LOGINTO(state,value){
        state.logindata=value
    },
    LOSE(state,value){
        state.logindata=value
    },
    CODEGET(state,value){
        state.codeerror=value
    },
    REGISTER(state,value){
        state.registererr=value
    },
    RETRIEVE(state,value){
        state.retrieveok=value
    }
}

const actions = {
      async  loginto({commit},value){
        let result=await  reqlogin(value);
        // console.log(result);
        if(result.msg=='success'){
            commit('LOGINTO',result.data)
        }else{
            commit('LOSE',result)
        }
        },
        async codeget({commit},value){
            let result=await reqcode(value);
            console.log(result);
            if(result.code='success'){
                commit('CODEGET','abc')
            }
            if(result.code='A0506'){
                commit('CODEGET',result.msg)
            }
        },
        async register({commit},value){
            let result=await reqregister(value);
            console.log(result)
            if(result.msg=='该用户已存在'){
                console.log('444')
                commit('REGISTER','err')
            }
            else{
                console.log('333')
                commit('REGISTER','1')
            }
        },
        async retrieve({commit},value){
            let result=await reqchange(value);
            console.log(result);
            if(result.msg=='success'){
                commit('RETRIEVE','ok')
            }else{
                commit('RETRIEVE','err')
            }
        }
}

export default {
    state,
    mutations,
    actions
}

